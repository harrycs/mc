import os 
import sys
import re
from subprocess import Popen, PIPE

from lib.cuckoo.common.exceptions import CuckooReportError 
from lib.cuckoo.common.abstracts import Report

class BSDump(Report):
    """Saves analysis results for BitShred."""

    def run(self, results):
        """Writes report.
        @param results: Cuckoo results dict.
        @raise CuckooReportError: if fails to write report.
        """
        try:

            sample_name = self.getSampleName(self.conf_path)

            report_file = os.path.join(self.reports_path, sample_name + ".ftu")
            self.f = open(report_file, 'w') 

            # import function
            self.getStatic(results) 
            
            self.getDropped(results, sample_name)
            self.getNetwork(results)
            self.getReg(results, sample_name)
            self.getMutex(results)
            self.getFiles(results, sample_name)
            if self.options["script"] != None:
                self.getAPI("modules/reporting/" + self.options["script"], sample_name)


#**************************************
#            self.getProcTree(results)
            
            self.f.close()

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate behavior report: %s" % e)

    def getAPI(self, script_path, matrix):
        """get getAPI entries"""
        sys.stderr.write("In getAPI !!!!!!!!\n")
        
        try:
            p = Popen(["perl", script_path, matrix], stdin = PIPE, stdout = PIPE)
            for file in os.listdir(self.analysis_path + "/logs/"):
                if file.endswith(".csv"):
                    file = self.analysis_path + "/logs/" + file
                    p.stdin.write(file + "\n")
            p.stdin.close()
            self.f.write(p.stdout.read())
        
        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate api report: %s" % e)


    def getSampleName(self, config):
        try:
            f = open(config, "r")
            for line in f:
                line = line.rstrip("\n")
                r = re.search("file_name = (?P<file_name>.+)$", line)
                if r != None:
                    sample_name = r.group("file_name")
                    break;


            f.close()
            return sample_name

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate get sample name: %s" % e)
 


    def getProcTree(self, result):
        """get process tree"""
        sys.stderr.write("In getProcTree !!!!!!!!\n")

        try:

            files  = result["behavior"]["processtree"]
            print files

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate proc tree report: %s" % e)



    def getFiles(self, result, sample_name):
        """get files touched"""
        sys.stderr.write("In getFiles !!!!!!!!\n")

        try:

            files  = result["behavior"]["summary"]["files"]
            if files == None:
                return
            
            for f in files:
                r = re.sub(sample_name,"$e|f", f)
                print >>self.f, "File touched:", r

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate files report: %s" % e)

 
 

    def getMutex(self, result):
        """get Mutex entries"""
        sys.stderr.write("In getMutex!!!!!!!!\n")
        
        try:
            mtxs = result["behavior"]["summary"]["mutexes"]
            if mtxs == None:
                return 

            for mtx in mtxs:
                print >>self.f, "Mutex: " + mtx
        
        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate Mutex report: %s" % e)
 

    def getReg(self, result, matrix):
        """get Registry entries"""
        sys.stderr.write("In getReg!!!!!!!!\n")
        
        try:
            keys = result["behavior"]["summary"]["keys"]
            if keys == None:
                return

            for key in keys:
                key_name = re.sub(matrix, "matr|x",key)
                print >>self.f, "RegKey: " + key_name
        
        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate registry report: %s" % e)
 

    def getNetwork(self, results):
        """get network behavior"""
        sys.stderr.write("In getNetwork!!!!!!!!\n")
        
        try:
            network = results["network"]
            if network == None:
                return 

            if network["udp"] != None:
                self.getUdp(network["udp"])
            if network["dns"] != None:
                self.getDns(network["dns"])
            #self.getHttp

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("failed to generate network report: %s" % e)
   

    def getDns(self, dnss):
        for dns_evnt in dnss:
            print >>self.f, "DNS:",

            for key in dns_evnt:
                print >>self.f, key,
                print >>self.f, dns_evnt[key],
            print >>self.f, ""

    def getUdp(self, udps):
        for udp_evnt in udps:
            print >>self.f, "UDP:",
            
            print >>self.f, "src",
            print >>self.f, udp_evnt["src"],
            print >>self.f, "sport",
            print >>self.f, udp_evnt["sport"],

            print >>self.f, "dst",
            print >>self.f, udp_evnt["dst"],
            print >>self.f, "dport",
            print >>self.f, udp_evnt["dport"]

    def getDropped(self, results, matrix):
        """get dropped files"""
        sys.stderr.write("In getDropped!!!!!!!!\n")
        try:
            dropped = results["dropped"]
            if dropped == None:
                return 

            #print dropped
            for ht in dropped:
                f_name = re.sub(matrix, "matr|x",ht["name"])
                print >>self.f, "Dropped: " + f_name + ",", ht["type"]
            

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("Failed to generate Dropped file eeport: %s" % e)



    def getStatic(self, results):
        """get static analysis result"""
        
        sys.stderr.write("In getStatic!!!!!!!!\n")

        try:
            static = results["static"]
            if static == None:
                return 
            
            for lst in static["pe_imports"]:
                dll_name = lst["dll"]
                imp_lst = lst["imports"]
                for imp_ht in imp_lst:
                    print >>self.f, "Import: " + dll_name,
                    print >>self.f, imp_ht["name"], imp_ht["address"]
                

        except (UnicodeError, TypeError, IOError) as e:
            raise CuckooReportError("Failed to generate Static analysis report: %s" % e)
