#!/usr/bin/perl -w

$pname = 3;
$fname = 7;
$arg0 = 10;


&main();



sub main
{
    ($matrix,) = @ARGV;
    &init_functable();

    while(<STDIN>)
    {
        chomp();
        $csv = $_;
    
        open IN, "<$csv";

#        %out_ht = ();
        &process_file();

        close IN;
    }
}

sub process_file
{
    while(<IN>)
    {
        &process_line($_);
    }
}

sub process_line
{
    my ($line,) = @_;
    @vec = split(',', $line);
    $vec[$fname] = substr($vec[$fname], 1, -1);

    $process_name = substr($vec[$pname], 1, -1);
  
    # abstract on unicode and ansi
    $fuc_name = $vec[$fname];
    if($fuc_name =~ /.+W$/ or $fuc_name =~ /.+A$/)
    {
        $fuc_name = substr($fuc_name, 0, -1);
    }

    $out = "API: " . $fuc_name;

    if(exists $func_table{$vec[$fname]})
    {
        &process_function();
    }

    $out = $out . "\n";
    if(not exists $out_ht{$out})
    {
        print $out;
        $out_ht{$out} = 1;
    }
}

sub process_function
{
    my $args = $func_table{$vec[$fname]};
  
    foreach $i (@$args)
    {
        my $argout = $vec[$arg0 + $i];
        chomp($argout);

        $argout =~ s/$process_name/\$s|f/g;
        $argout =~ s/$matrix/matr|X/g;
        $out = $out . ", " . $argout;
    }
}

sub init_functable
{
    %func_table = (
        "LdrLoadDll" => [0, 1],
        "LdrGetDllHandle" => [0],
        "LdrGetProcedureAddress" => [1],
        "IsDebuggerPresent" => [],
        "RegOpenKeyExW" => [0,1],
        "RegOpenKeyExA" => [0,1],
        "RegCreateKeyExA" => [0,1],
        "RegCreateKeyExW" => [0,1],
        "RegEnumKeyExA" => [1,2],
        "RegEnumKeyExW" => [1,2],
        "RegEnumValueW" => [1,2,3],
        "NtOpenKey" => [2],
        "NtQueryValueKey" => [1],
        "RegQueryValueExW" => [1],
        "RegQueryInfoKeyW" => [],
        "RegSetValueExW" => [1,2,3],
        "RegSetValueExA" => [1,2,3],
        "RegDeleteKeyA" => [1],
        "RegDeleteKeyW" => [1],
        "RegCloseKey" => [],
        "NtOpenFile" => [2],
        "NtCreateFile" => [2],
        "NtReadFile" => [],
        "NtWriteFile" => [],
        "NtSetInformationFile" => [],
        "NtQueryInformationFile" => [],
        "CopyFileExA" => [0,1],
        "CopyFileExW" => [0,1],
        "CreateDirectoryW" => [0],
        "DeleteFileA" => [0],
        "DeleteFileW" => [0],
        "FindFirstFileExW" => [0],
        "NtOpenProcess" => [],
        "ExitProcess" => [],
        "ShellExecuteExW" => [0, 1],
        "CreateThread" => [],
        "CreateRemoteThread" => [],
        "NtResumeThread" => [],
        "WriteConsoleW" => [],
        "OpenSCManagerW" => [],
        "OpenSCManagerA" => [],
        "OpenServiceW" => [1],
        "NtDelayExecution" => [0],
        "DeviceIoControl" => [1],
        "NtAllocateVirtualMemory" => [],
        "NtOpenSection" => [2],
        "NtCreateSection" => [2],
        "FindWindowExA" => [1],
        "SetWindowsHookExA" => [0],
        "HttpOpenRequestA" => [1],
        "InternetConnectA" => [1, 2, 3, 4, 5],
        "InternetOpenA" => [0],
        "CreateProcessInternalW" => [0, 1]

    );

}
