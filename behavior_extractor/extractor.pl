#!/usr/bin/perl -w

$fname = 7;
$arg0 = 10;

&main();




sub main
{
    &init_functable();

# csv files in one folder will be merged for one sample
    $curr_folder = "";
    $pre_folder = "";

    while(<>)
    {
        chomp();
        $csv = $_;
        open IN, "<$csv";

# get current folder, better to use liabrary?
        if(/^(\/.+\/).+\.csv$/)
        {
            $curr_folder = $1;
        }

# if folder changes, one sample has been processed        
        if($curr_folder ne $pre_folder)
        {
            if(defined OUT)
            {
                close OUT;
            }
            my $behav = $curr_folder."API.behav";
            print $behav . "\n";

            open OUT, ">$behav";
            $pre_folder = $curr_folder;

# each sample has a hash to avoid repeat output
            %out_ht = ();
        }

        &process_file();

        close IN;
    }
    close OUT;
}

sub process_file
{
    while(<IN>)
    {
        &process_line($_);
    }
}

sub process_line
{
    my ($line,) = @_;
    @vec = split(',', $line);
    $vec[$fname] = substr($vec[$fname], 1, -1);
    
    $out = $vec[$fname];

    if(exists $func_table{$vec[$fname]})
    {
        &process_function();
    }

    $out = $out . "\n";
    if(not exists $out_ht{$out})
    {
        print OUT $out;
        $out_ht{$out} = 1;
    }
}

sub process_function
{
    my $args = $func_table{$vec[$fname]};
  
    foreach $i (@$args)
    {
        my $argout = $vec[$arg0 + $i];
        chomp($argout);
        $out = $out . ", " . $argout;
    }
}

sub init_functable
{
    %func_table = (
        "LdrLoadDll" => [0, 1],
        "LdrGetDllHandle" => [0],
        "LdrGetProcedureAddress" => [1],
        "IsDebuggerPresent" => [],
        "RegOpenKeyExW" => [0,1],
        "RegOpenKeyExA" => [0,1],
        "RegCreateKeyExA" => [0,1],
        "RegCreateKeyExW" => [0,1],
        "RegEnumKeyExA" => [1,2],
        "RegEnumKeyExW" => [1,2],
        "RegEnumValueW" => [1,2,3],
        "NtOpenKey" => [2],
        "NtQueryValueKey" => [1],
        "RegQueryValueExW" => [1],
        "RegQueryInfoKeyW" => [],
        "RegSetValueExW" => [1,2,3],
        "RegSetValueExA" => [1,2,3],
        "RegDeleteKeyA" => [1],
        "RegDeleteKeyW" => [1],
        "RegCloseKey" => [],
        "NtOpenFile" => [2],
        "NtCreateFile" => [2],
        "NtReadFile" => [],
        "NtWriteFile" => [],
        "NtSetInformationFile" => [],
        "NtQueryInformationFile" => [],
        "CopyFileExA" => [0,1],
        "CopyFileExW" => [0,1],
        "CreateDirectoryW" => [0],
        "DeleteFileA" => [0],
        "DeleteFileW" => [0],
        "FindFirstFileExW" => [0],
        "NtOpenProcess" => [],
        "ExitProcess" => [],
        "ShellExecuteExW" => [0, 1],
        "CreateThread" => [],
        "CreateRemoteThread" => [],
        "NtResumeThread" => [],
        "WriteConsoleW" => [],
        "OpenSCManagerW" => [],
        "OpenSCManagerA" => [],
        "OpenServiceW" => [1],
        "NtDelayExecution" => [0],
        "DeviceIoControl" => [1],
        "NtAllocateVirtualMemory" => [],
        "NtOpenSection" => [2],
        "NtCreateSection" => [2],
        "FindWindowExA" => [1],
        "SetWindowsHookExA" => [0]
    );

}
