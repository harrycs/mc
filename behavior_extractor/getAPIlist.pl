#!/usr/bin/perl

while(<>)
{
    if(/^(\w+)\s+(\[.*\])/)
    {
        chomp($1);
        print '"' . $1 . '"';
        print " => ";
        print $2;
        print ",\n";
    }
}
