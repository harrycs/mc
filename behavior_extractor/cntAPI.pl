#!/usr/bin/perl -w

&main();


sub main()
{
    while($file = <STDIN>)
    {
        chomp($file);
        open IN, "<$file";
        while(<IN>)
        {
            chomp();
            @strs = split(",");
            if(not exists $ht{$strs[7]})
            {
                $ht{$strs[7]} = 1;
            }
            else
            {
                $ht{$strs[7]} = $ht{$strs[7]} + 1;
            }
        }

    }

    foreach $f (sort keys %ht)
    {
#        print $f . ' ' . $ht{$f} . "\n";
         my $vec;
         if(not exists $lst{$ht{$f}})
         {
            $vec = [];
            $lst{$ht{$f}} = $vec;
         }
         else
         {
            $vec = $lst{$ht{$f}};
         }
         
         push(@$vec, $f);
    }

    foreach $num (reverse sort {$a<=>$b} keys %lst)
    {
        print $num . ' ';
        my $vec = $lst{$num};
        foreach $f (@$vec)
        {
            print $f . ' ';
        }
        print "\n";
    }

}
