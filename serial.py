#!/usr/bin/python

import pickle
import cPickle
import json
from pprint import pprint
import re

import os


def loadRes():
    arr = []
    with open("vt_labels.txt", "r") as f:
        for line in f:
            data = json.loads(line)
            arr.append(data)
    
    return arr

def findInvalid(rmv, dir_name):
    tcnt = 0
#    for filename in os.listdir("/home/harry/Documents/samples"):
    for filename in os.listdir(dir_name):
        #print filename
        if filename in rmv:
            tcnt = tcnt + 1

    print tcnt

def getInvalid(result_lst):
    tcnt = 0
    rmv = {}
    for smp in result_lst:
        cnt = 0;
        for av in smp["scans"]:
            if smp["scans"][av]["detected"] == True:
                cnt = cnt + 1;
        if cnt < 25:
            tcnt = tcnt + 1
            #print smp["md5"]
            rmv[smp["md5"]] = 1
    print tcnt
    return rmv
    
def removeMinor(result_lst):
    for smp in result_lst:
        cnt = 0
        for av in smp["scans"]:
            if smp["scans"][av]["detected"] == True:
                cnt = cnt + 1
        if cnt < 20:
            result_lst.remove(smp) 

def cntAVApp(result_lst, AV_names):
    aim = len(AV_names)
    tcnt = 0
    for smp in result_lst:
        cnt = 0
        for name in AV_names:
            if name in smp["scans"]:
                cnt = cnt + 1 
        if cnt == aim:
            tcnt = tcnt + 1
    return tcnt

def removeAVApp(result_lst, AV_names):
    rmlst = []
    aim = len(AV_names)
    for smp in result_lst:
        cnt = 0
        for name in AV_names:
            if name in smp["scans"] and smp["scans"][name]["detected"] == True:
                cnt = cnt + 1 
        if cnt != aim:
            rmlst.append(smp)

    for rm in rmlst:
        result_lst.remove(rm)


result = loadRes()
removeMinor(result)
print len(result)
AVs = ["Kaspersky", "Microsoft", "Symantec", "NOD32", "BitDefender", "McAfee"]
removeAVApp(result, AVs)
print len(result)
for smp in result:
    print smp["md5"]
    for AV in AVs:
        print smp["scans"][AV]["result"]
    print 
