#!/usr/bin/perl -w
use File::Copy;
use File::Basename;

&main();


sub main
{
    if(@ARGV != 1)
    {
        die "usage: ./getAllFtu.pl <output_folder>";
    }

    my ($out_folder,) = @ARGV;

    if(not -d $out_folder)
    {
        die "not a valid directory";
    }
    while(<STDIN>)
    {
        chomp();
        my $file_path = $_;
        my ($name, $folder, $ext) = fileparse($file_path);
        copy($file_path, $out_folder . $name . $ext);
    }
}


