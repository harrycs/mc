#!/usr/bin/python

import pickle
import cPickle
import json
from pprint import pprint
import re

import os



def loadRes():
    arr = []
    with open("vt_labels.txt", "r") as f:
        for line in f:
            data = json.loads(line)
            arr.append(data)
    
    return arr


def getMd5s(result_lst):
    md5s = {}
    for smp in result_lst:
        md5s[smp["md5"]] = 1
    return md5s
    

def removeInvalidFile(validMd5s, dir_name):
    for filename in os.listdir(dir_name):
        m = re.search("(?P<name>.+)\.ftu$", filename)
        if m != None:
            if m.group("name") not in validMd5s:
                print filename
                os.remove(dir_name + "/" + filename)
#        if filename not in result_lst:
#            tcnt = tcnt + 1


    
def removeMinor(result_lst):
    for smp in result_lst:
        cnt = 0
        for av in smp["scans"]:
            if smp["scans"][av]["detected"] == True:
                cnt = cnt + 1
        if cnt < 20:
            result_lst.remove(smp) 

def cntAVApp(result_lst, AV_names):
    aim = len(AV_names)
    tcnt = 0
    for smp in result_lst:
        cnt = 0
        for name in AV_names:
            if name in smp["scans"]:
                cnt = cnt + 1 
        if cnt == aim:
            tcnt = tcnt + 1
    return tcnt

def removeAVApp(result_lst, AV_names):
    rmlst = []
    aim = len(AV_names)
    for smp in result_lst:
        cnt = 0
        for name in AV_names:
            if name in smp["scans"] and smp["scans"][name]["detected"] == True:
                cnt = cnt + 1 
        if cnt != aim:
            rmlst.append(smp)

    for rm in rmlst:
        result_lst.remove(rm)

def getAVResult(result_lst, AV_name):
    res = {}

    if AV_name == "Kaspersky":
        pat = "(.*:)*(?P<type>[^\.]+\.[^\.]+\.[^\.]+)"
        #"(.*:)*(?P<type>[^\.]+\.[^\.]+)"
    elif AV_name == "Microsoft":
        pat = "(?P<type>[^\.]+)" 
    #    pat = "(?P<type>[^\.!]+)" 
    elif AV_name == "Symantec":
        pat = "(?P<type>[^!]+)" 
    elif AV_name == "NOD32":
        pat = "(?P<type>[^\.]+(\.[^\.]){0,1})"
    elif AV_name == "BitDefender":
        pat = "(?P<type>[^\.]+\.[^\.]+)"
    elif AV_name == "McAfee":
        pat = "(?P<type>[^!]+)"
    else:
        return 
    
    res = {}
    for smp in result_lst:
        m = re.search(pat, smp["scans"][AV_name]["result"])
        res[smp["md5"]] = m.group("type")

    return res
    
   
    
def getSortAVResult(result_lst, AV_name):

    if AV_name == "Kaspersky":
        pat = "(.*:)*(?P<type>[^\.]+\.[^\.]+\.[^\.]+)"
        #"(.*:)*(?P<type>[^\.]+\.[^\.]+)"
    elif AV_name == "Microsoft":
        pat = "(?P<type>[^\.]+)" 
    #    pat = "(?P<type>[^\.!]+)" 
    elif AV_name == "Symantec":
        pat = "(?P<type>[^!]+)" 
    elif AV_name == "NOD32":
        pat = "(?P<type>[^\.]+(\.[^\.]){0,1})"
    elif AV_name == "BitDefender":
        pat = "(?P<type>[^\.]+\.[^\.]+)"
    elif AV_name == "McAfee":
        pat = "(?P<type>[^!]+)"
    else:
        return 

    f = open(AV_name + ".txt", "w")
    print >>f, "#" + AV_name
    
    res = {}
    for smp in result_lst:
        m = re.search(pat, smp["scans"][AV_name]["result"])
        if m.group("type") not in res:
            md5s = []
            res[m.group("type")] = md5s
        else:
            md5s = res[m.group("type")]
        md5s.append(smp["md5"])
    
    keys = res.keys()
    keys.sort()
    for key in keys:
#        cnt = 1
        md5s = res[key]
        for md5 in md5s:
#            print >>f, key + "-" + str(cnt), md5
#            cnt = cnt + 1
            print >>f, key, md5

    f.close()


def getMutiAVResult(result_lst, AV_names):
    for smp in result_lst:
        print smp["md5"]
        for AV_name in AV_names:
            print smp["scans"][AV_name]["result"]

        print

def genRef(vdb_list ,ftu_dir_name, AV_name, AV_result):
    f = open(vdb_list, "r")
    nmap = {}
    for line in f:
        m = re.search("(\d+)\s+([0-9a-zA-Z]+)", line) 
        if m != None:
            nmap[m.group(2)] = m.group(1)
    f.close()
   
    f = open(AV_name + "_ref.txt", "w")
    clss = {}
    for filename in os.listdir(ftu_dir_name):
        m = re.search("(?P<name>.+)\.ftu$", filename)
        if m != None:
            num = nmap[m.group("name")]
            typ = AV_result[m.group("name")]
            if typ not in clss:
                clss[typ] = []
            vec = clss[typ]
            vec.append(num)

    cnt = 0
    for cls in clss:
        print >>f, "C" + str(cnt) + ":",
        for s in clss[cls]:
            print >>f, s,

        print >>f, "" 
        cnt = cnt + 1

    f.close()


AVs = ["Kaspersky", "Microsoft", "Symantec", "NOD32", "BitDefender", "McAfee"]

result = loadRes()
removeMinor(result)
print len(result)
removeAVApp(result, AVs)
print len(result)
md5s = getMd5s(result)
ftu_dir = "/home/harry/Documents/partial_result/feature"


#getMutiAVResult(result, AVs)
#av_res = getAVResult(result, "Microsoft")

#for AV in AVs:
#    getSortAVResult(result, AV)

#removeInvalidFile(md5s, ftu_dir)
for AV in AVs:
    av_res = getAVResult(result, AV)
    genRef("/home/harry/Documents/bitshred_single-0.1/src/db/vdb_list.txt", ftu_dir, AV, av_res)
